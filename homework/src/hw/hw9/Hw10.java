package hw.hw9;

public class Hw10 {
    public static void main(String[] args) {
        Movable square = new Square(10, 10);
        Movable circle = new Circle(10);
        square.move(10, 10);
        circle.move(10, 10);
    }
}
