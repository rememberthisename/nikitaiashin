package hw.hw9;

public class Square extends Rectangle implements Movable {

    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public void move(int xAxis, int yAxis) {
        this.x += xAxis;
        this.y += yAxis;
    }
}
