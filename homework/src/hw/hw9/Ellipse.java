package hw.hw9;

public class Ellipse extends Figure {
    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    protected int getPerimeter() {
        return (int) (2 * 3.14 * Math.sqrt( ( (x* y) + (x * y) ) / 2));
    }
}
