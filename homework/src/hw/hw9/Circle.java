package hw.hw9;

public class Circle extends Ellipse implements Movable{

    public Circle(int x) {
        super(x, 0);
    }

    @Override
    protected int getPerimeter() {
        return (int) (2 * Math.PI * x);
    }

    @Override
    public void move(int xAxis, int yAxis) {
        this.y += yAxis;
        this.x += xAxis;
    }
}
