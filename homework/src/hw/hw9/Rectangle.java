package hw.hw9;

public class Rectangle extends Figure {

    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    protected int getPerimeter() {
        return 2 * (x + y);
    }
}
