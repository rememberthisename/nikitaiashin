package hw.hw9;

public interface Movable {
    void move(int xAxis, int yAxis);
}
