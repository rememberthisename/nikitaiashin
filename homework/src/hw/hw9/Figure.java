package hw.hw9;

public abstract class Figure {

    protected int x;
    protected int y;

    protected Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    protected int getPerimeter() {
        return 0;
    }
}
