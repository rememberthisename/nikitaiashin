package hw.hw5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class HomeWork5 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        List<Integer> lines = new ArrayList<>();
        String input;
        while (true) {
            input = scan.nextLine();
            if (input.equals("-1")) {
                break;
            }
            lines.add(Integer.valueOf(input));
        }
        System.out.println(Collections.min(lines));
    }
}