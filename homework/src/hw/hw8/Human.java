package hw.hw8;

public class Human {

    protected String name;
    protected int weight;

    protected Human(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return this.name + " " + this.weight;
    }
}
