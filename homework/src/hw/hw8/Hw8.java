package hw.hw8;

import java.util.Arrays;

public class Hw8 {

    public static void main(String[] args) {
        //На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
        Human[] humans = new Human[]{
                new Human("Alex", 90),
                new Human("Stas", 70),
                new Human("Olga", 99),
                new Human("Masha", 80),
                new Human("Tolik", 50),
                new Human("Oleg", 120),
                new Human("Ira", 33),
                new Human("Igor", 65),
                new Human("Mark", 111),
                new Human("Oksana", 45),
                new Human("Ilia", 55)
        };
        Arrays.sort(humans, new SortByWeight());
        System.out.println(Arrays.toString(humans));
    }


}
