package hw.hw10;

public class Logger {

    private static final int MAX_LOGS_COUNT = 1000;
    private String[] logs;
    private int count;

    private static final Logger logger;

    static {
        logger = new Logger();
    }

    private Logger(){
        this.logs = new String[MAX_LOGS_COUNT];
    }

    public void add(String log) {
        if (count < MAX_LOGS_COUNT) {
            this.logs[count] = log;
            count++;
        } else {
            System.err.println("Log Overflow");
        }
    }
}
