package hw.hw7;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Hw7 {

    private static int MAX_VALUE = 101;

    static int[] readInputValues() {
        Scanner scan = new Scanner(System.in);
        List<Integer> lines = new ArrayList<>();
        String input;
        while (true) {
            input = scan.nextLine();
            if (input.equals("-1")) {
                break;
            }
            lines.add(Integer.valueOf(input));
        }
        return lines.stream().mapToInt(Integer::intValue).toArray();
    }

    static int maxRepeating(int arr[], int n, int k) {
        for (int i = 0; i < n; i++){
            arr[(arr[i] % k)] += k;
        }
        int max = arr[0], result = 0;
        for (int i = 1; i < n; i++) {
            if (arr[i] > max) {
                max = arr[i];
                result = i;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] arr = readInputValues();
        int n = arr.length;
        int k=MAX_VALUE;
        System.out.println("Maximum repeating element is: " + maxRepeating(arr,n,k));
    }
}
